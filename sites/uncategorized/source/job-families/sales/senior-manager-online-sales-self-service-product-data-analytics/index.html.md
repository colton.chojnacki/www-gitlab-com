---
layout: job_family_page
title: Senior Manager, Online Sales & Self Service Product & Data Analytics
---

The Senior Manager, Online Sales & Self Service Product & Data Analytics job family is responsible for all Product & Data Analytics solutions driving the growth and maturity of Self Service at GitLab.

## Role
The Senior Manager, Online Sales & Self Service Product & Data Analytics reports to the [VP Online Sales and Self Service](https://about.gitlab.com/job-families/sales/vice-president-online-sales-and-self-service/).

### Job Grade
The Senior Manager, Online Sales & Self Service Product & Data Analytics is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
* Lead product & data analytics for Self Service to drive revenue growth, sales efficiency, customer experience, and customer retention.
* Develop and execute a product & data analytics solution that will create a full data funnel: product interactions to revenue, establishing and tracking success metrics and aiding in feature prioritization.
* Through data insights provide sales visibility, inform product roadmap, drive marketing campaigns & customer interactions.
* Build and execute an experimentation engine to inform marketing campaigns, product roadmap and customer interactions.
* Partner closely with Data Instrumentation, Data Science, Product Management, Fulfillment to establish data pipelines to provide a comprehensive funnel view.
* Partner closely with Sales, Marketing, Product Management, Fulfillment, Sales Strategy and Support teams to provide Self Service data insights to drive focus.
* Establish and maintain key customer KPIs for Self Service GTM success including conversion, retention and growth.
* Provide detailed and accurate sales forecasting for online sales by segment, channel, and region.
* Maintain source of truth for product analytics insights.
* Ensure support and product teams have visibility into error data to improve the customer experience.
* Provide application monitoring to understand impacts to usage: availability, uptime, performance.
* Build a team of data analysts, data engineers and data architects to deliver our Self Service product & analytics solution.
* Manage prioritization of iteration of online sales funnel and instrumentation.

### Requirements
* Ability to use GitLab
* A solid track record of Product & Data Analytics delivery and leadership
* Has specialized knowledge of Product & Data Analytics technology tools and implementation
* Strong aptitude in data analytics and advanced statistical modelling
* Has specialized knowledge of the Online Sales, Self Service/e-commerce space, and customers
* Strong experience in software sales, preferably with development tool and/or open-source experience 
* Proven success partnering with Marketing, Channel, Alliances, Product, and Engineering peers 
* Demonstration of high levels of integrity, initiative, honesty and leadership 
* Must be adaptable, professional, courteous, motivated and work well on their own or as a member of a team 
* Willingness to learn and use GitLab 
* Polished presentation skills 
* Ability to handle a fast-paced environment and ambitious workload

### Performance Indicators
* CAC Ratio (target: Monthly target set by Finance)
* Percent of Ramped Reps at or Above Quota (target: greater than 55%)
* Percent of Ramping Reps at or Above 70% of Quota (target: greater than 70%)
* Churn and Contraction / ATR (target: Quarterly target set by Sales & Finance)
* ARPU (target: 1.5% increase per month)

### Career Ladder
The next step for the Senior Manager, Online Sales & Self Service Product & Data Analytics Job Family is to move to the Director, Online Sales & Self Service Job Family. 

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.

* Qualified candidates will be invited to schedule a 30 minute screening call with our VP, Talent Acquisition.
* Next, candidates will be invited to schedule a first interview with the Hiring Manager
* Next, candidates will be invited to schedule interviews with members of the CRO Leadership team
* Next, candidates will be invited to scheduled interviews with members of our Fulfillment and Growth teams
* Finally, candidates will be asked to interview with our Chief Revenue Officer
