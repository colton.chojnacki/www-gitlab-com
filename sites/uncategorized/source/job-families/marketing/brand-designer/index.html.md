---
layout: job_family_page
title: "Brand Designer"
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/nBSQmB_ruco" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Brand Designer

At GitLab, Brand Designers are responsible for ensuring strategic, seamless experiences - working closely with Marketing, Sales, and Product teams to elevate our global brand and optimize the customer journey end-to-end.


### Responsibilities

- Define and create a visual language as an extension of the brand (illustrations, icons, colors, typography, animations, etc.)
- Be an advocate for consistent and cohesive design throughout the company.
- Contribute to the [Pajamas Design System](https://design.gitlab.com/); specifically the Brand and Marketing sections.
- Educate others on design thinking, brand experience, and design-driven storytelling.
- Collaborate with Content Marketing, and others, to establish a clear and concise brand voice, tone, and personality.
- Collaborate with UX Design to create and maintain a cohesive and consistent brand experience between Marketing and Product.
- Continually iterate on concepts in an effort to make the brand relevant and relatable to our audience(s).
- Strong ability to prioritize work and resources across various projects.


### Requirements

- Effectively communicate conceptual ideas and design rationale.
- Engage in constructive design critiques.
- Able to work independently, prioritize accordingly, and iterate quickly.
- Strong communication skills without a fear of over communication.
- You share our [values](/handbook/values/), and work in accordance with those values.
- Ability to use GitLab

## Levels

### Associate Brand Designer

- Execute design and conceptual tasks as assigned with significant attention to detail.
- Learn the GitLab brand, visual language, and the [Pajamas Design System](https://design.gitlab.com/).
- Contribute to, and collaborate with fellow designers, on design concepts and the GitLab brand experience.

#### Job Grade

The Associate Brand Designer is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Requirements

- 1–3 years experience in a design-related role.
- Ability to continuously meet deadlines and execute on projects.

### Intermediate Brand Designer

- Develop visual concepts for digital campaigns, webpages, print, and events.
- Enable others, non-designers, to create on-brand assets.
- Actively contribute to the [Pajamas Design System](https://design.gitlab.com/).

#### Job Grade

The Intermediate Brand Designer is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Requirements

- 3–5 years experience in a design-related role.
- Proven ability to independently conceptualize and think critically about design.

### Senior Brand Designer

Everything in the Brand Designer role, plus:

- Generate clear ideas and concepts in tandem with peers and stakeholders.
- Lead cross-discipline collaboration as it relates to brand experience (Sales, Content, Product, etc.).
- Understand marketing initiatives, strategic positioning and target audience.
- Cooperate with the rest of the creative team across different types of media.
- Facilitate iterative and inclusive design process end-to-end within deadlines.
- Effectively present ideas to clients/team members.
- Stay on top of brand, design, media and conversion best practices.
- Be an advocate for design; educating and enabling others to create on-brand experiences.

#### Job Grade

The Senior Brand Designer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Requirements

- 7 years agency experience (marketing, design or advertising), or on a design team at a technology company.
- Proven experience designing strategic omni-channel experiences at an agency or technology company. SaaS experience preferred.
- Must be fluent in brand, marketing and design principles and best practices.
- Hands on experience with logo design, typography, color, web layout design, print production, photography, and more.
- Proficient use of InDesign, Photoshop, and Illustrator.
- Demonstrable design thinking and execution skills with a strong portfolio.
- Incorporate feedback and take/give direction well.
- Team player with strong communication and presentation skills.
- BA/BS in design, communications, marketing, HCI, or related experience is strongly preferred.

#### Bonus

- Project or product management experience.
- Product marketing experience.
- Knowledge of Git or GitLab.
- Technology marketing experience.

### Manager, Brand Design

Everything in the Senior Brand Designer role, plus:

- Lead the Brand Design team fostering innovation, collaboration, and continual evolution of GitLab's brand experience.
- Oversee the definition, prioritization, and improvement of the Brand and Marketing sections of the [Pajamas Design System](https://design.gitlab.com/).
- Research and develop a greater understanding of GitLab's audience(s); iterating to keep our brand experience relevant and relatable.

#### Job Grade

The Manager, Brand Design is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Requirements

- 5+ years experience in branding and marketing design.
- Degree in Graphic Design, Marketing, or equivalent work experience.
- Excellent communication, organizational, and leadership capabilities.
- Proven experience leading designers and successfully maintaining and evolving brand experience in all aspects.

## Career Ladder

The next step in the Brand Design job family is not yet defined at GitLab. 
