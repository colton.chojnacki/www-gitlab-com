---
layout: handbook-page-toc
title: "Change Management Policy"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<!-- HTML Parsing for Panel Style Alerts -->
{::options parse_block_html="true" /}

# Purpose

The purpose of the Change Management Policy is to ensure that a standard set of minimum requirements are established for changes that are made to production systems and supporting infrastructure across the organization. These requirements are meant to provide a level of consistency across how changes are managemented from the initial change request through to production deployment. These requirements have been established based on the 

# Scope

Changes, in the context of this policy, are defined as **modifications** to the production environments and supporting infrastructure applies to changes that are made to systems assigned a [Critical System Tier](/handbook/engineering/security/security-assurance/security-risk/storm-program/critical-systems.html) of `Tier 1 Product`, `Tier 1 Business`, `Tier 2 Core`, and `Tier 2 Support`. These **modifications** include:
- Changes to configurations (e.g. changes to configured password requirements, batch jobs or pipeline schedules, report configurations/calculations for reports utilized in key business processes, turning on or off functionality available within a system, etc.)
- The creation/development/implementation of a new system, new system integrations, system features, key reports, databases, etc.
- Applying patches or vendor supplied changes
- Modifications to data schemas
- Deprecating a system and replacing it with a new system
- Broadly speaking, any change that will impact how team members carry out their responsibilities

**Note:** While Tier 3 / Non-critical systems are not subject to the scope of this policy, team members are encouraged to proactively adopt the requirements established by this policy across all systems, especially if there is a good probability that a system may move from a `Tier 3 / Non-Critical` system to a higher system tier. As a summary, critical system tiers are determined based on the importance of the system in supporting the delivery of the GitLab.com Product, critical internal business functions (such as payroll), the types of systems that a system integrates with, the extent of GitLab's reliance on the system to facilitate execution of integrations or scheduled jobs, and even data classification. Additional information about critical system tiers can be found on the [Critical System Tiering Methodology](/handbook/engineering/security/security-assurance/security-risk/storm-program/critical-systems.html) hamdbook page.
{: .note}


# Roles & Responsibilities

|Role|Responsibility(ies)|
|----------|---------------|
|Security Compliance Team|Responsible for the continuous monitoring of change management procedures across the relevant systems through security controls testing to ascertain adherence to this policy|
|Technical System Owners<br><br>Business System Owners<br><br>System Administrators|Responsible for ensuring the minimum requirements established by this policy are implemented and executed consistenly across the systems they own and/or administer|
|Team Members|Responsible for executing change management procedures in a way that aligns with this policy and any additional subsequent procedures established by the department/function governing a system|
|Control Owners|Responsible for defining and implementing change management procedures that meet or exceed the minimum requirements that have been established by this policy|

# Minimum Change Management Requirements

The minimum change management requirements described below have been identified based on the [Change Management Control Family (CHG)](/handbook/engineering/security/security-assurance/security-compliance/guidance/change-management.html) established by the [GitLab Control Framework (GCF)](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html#gitlab-control-framework-gcf). These controls are subject to external audit procedures and therefore provide the minimum requirements for change management across the organization.

## Documented Change Procedures/Workflow

In conjunction with this policy, a supplemental change management process/procedure is formally documented and describes the standard operating procedure/workflow for executing changes over a specified scope of production systems and infrastructure. This process/procedure is documented in accordance with the [Controlled Document Procedure](/handbook/engineering/security/controlled-document-procedure.html). These procedures must incorporate the requirements called out in the sections below:
   - [Change Request Documentation Requirements](#change-request-documentation-requirements)
   - [Change Testing Requirements](#change-testing-requirements)
   - [Change Review and Approval Requirements](#change-review-and-approval-requirements)
   - [Change Deployment Requirements](#change-deployment-requirements)

### Change Request Documentation Requirements
- Details on why the change is being requested
- Information about the Impact of the Change (e.g. does the change impact team members? and if so, in what capacity?)
- Testing procedures, including the [change testing requirements](#change-testing-requirements) documented below
- Change type, if applicable (e.g. teams may opt to have different procedures for emergency changes, standard changes, configuration changes, etc. or use a singular procedure for all changes, regardless of the type of change)
- Change rollback/backout procedures in the event a deployed change does not function as intended
- Spepcific requirements for peer review(s), approvals, and post-deployment verifcation, as applicable (e.g requirements may be based on change type, as long as the change review and approvals meet minimum requirements outlined in the related section below)
- Change Communication: who are the relevant stakeholders that need to be notified of the change (e.g. if the change impacts all team members, how will team members be notified?)

### Change Testing Requirements

- Where possible, changes are tested in a non-production environment. The exception to this requirement would be changes to systems in which GitLab does not have a environment separate from production. Team members are required to leverage test accounts/test data where feasible if testing must be conducted in a production system due to lack of availability of a non-production environment.
- Documented testing requirements / test plans, which consider the following, as applicable:
   - security testing (e.g. if there is sensitive data involved)
   - manual testing / reconciliations (e.g. new finance report created to support month end activities - testing should include verifying calculated totals are complete and accurate and intended account data is included)
   - configurations changes are tested to confirm they function as intended
   - any additional testing procedures applicable based on the nature of the change
- Testing is documented, team members who performed testing, the results and evidence of testing is attached/linked back to the change request

### Change Review and Approval Requirements

- Once developed and tested, a change is formally reviewed and approved. Review(s) and approval(s) meet the following requirements:
   - Reviewer(s) and approver(s) must be different than the team members who worked on/developed the change
   - Reviewer(s) and approver(s) must not make changes to the change that do not go through additional approval/review by another individual
   - Where technically feasible, tools or systems utilized to develop, test, and approve changes should be configured in a way that prohibits unauthorized changes to be made without the appropriate approval(s). Subsequently, configurations should also restrict the ability for team members who developed and/or tested a change from being able to approve their change.
- Reviewer(s) and Approver(s), alongside dates of review and approval, are documented on the change request to ensure auditability

### Change Deployment Requirements

- Where possible, changes are deployed by a team member separate than the team member(s) who developed the change. If this is not possible, team members must confirm that additional changes/development work does not take place after the change has been approved. If additional work is completed after a change has been approved for deployment, additional review(s) and approval(s) should be obtained prior to the change being deployed. 
- Date of deployment is documented on the change request to ensure auditability
- Where possible, post-deployment verifications are completed and comments/documentation are linked to the change request (e.g. change is successful and functioning as anticipated in production)

## Example Workflow 

In order to provide team members with the relevant data needed for success in the creation, maintenance, and execution of change procedures they are responsible for, the information in this section provides an exmaple of how to incorporate the requirements of this policy into a change workflow.

1. Change requests can be tied back to a specific change or changes from the system (i.e as part of external audits, an auditor will ask for a population of changes from the SYSTEM itself instead of the change request repository. A sample of changes will be requested from this population and for each change, the related change request/change documentation will need to be provided)
2. Where it is not technically feasible for systems to be configured in a way that restricts the ability for those involved with the development of a change from being able to deploy a change, team members should consider some sort of periodic review of changes to ensure that changes are not being deployed by any individual without an appropriate separate individual's approval (or identify an alternative mechanism or process to monitor for this)
3. Some minimum requirements may not be applicable based on the type of change (e.g. emergency changes typically don't require approvals prior to deployment due to the need to deploy emergency changes quickly). Change procedures should clearly document exceptions to the minimum requirements in this policy and the mechanisms in place to meet the requirements of this policy (e.g. for emergency changes, a post-deployment verification is performed and retroactive approvals are obtained and documented on the emergency change request within a specified period of time)

Here is an example change management workflow diagram that includes different change types:

```mermaid
graph TD;
  cmProcedure([Change Procedures<br>Established])
  cmR[Open Change Request]
  cmReq[Change Requirements<br/>Documented]
  cmType{Type of<br>Change}
  cmType1[Standard Change]
  cmType2[Emergency Change]
  cmEmerFlow[[Emergency Change<br>Workflow]]
  cmType3[Vendor Supplied Change]
  cmDevelop[Change is Developed]
  cmTest[Change is Tested]
  cmTestResults{Results of<br>Testing}
  cmFix["Testing failure(s)<br>investigated and<br>fixed"]
  cmRevApp[Change is Reviewed and Approved]
  cmDeployed[Change is Deployed]
  cmClose[Change Request Closed/Completed]

  cmProcedure --> cmR --> cmReq --> cmType
  cmType --> cmType1
  cmType --> cmType2
  cmType --> cmType3
  cmType1 --> cmDevelop --> cmTest --> cmTestResults
  cmType2 --> cmEmerFlow 
  cmType3 --> cmTest 
  cmTestResults --> |Successful|cmRevApp --> cmDeployed --> cmClose
  cmTestResults --> |Failed|cmFix --> cmTest
```

<div class="panel panel-gitlab-orange">
**When in doubt, consult with Security Assurance**
{: .panel-heading}
<div class="panel-body">

Team members who have questions about the minimum requirements in this policy or the appropriateness of change procedures that they maintain should consult with the [Security Assurance Team](/handbook/engineering/security/security-assurance/security-assurance.html) as needed.

</div>
</div>

# Exceptions

Exceptions to this procedure will be tracked as per the [Information Security Policy Exception Management Process](/handbook/engineering/security/#information-security-policy-exception-management-process).

# References

- [Controlled Document Procedure](/handbook/engineering/security/controlled-document-procedure.html)
- [Critical System Tiers](/handbook/engineering/security/security-assurance/security-risk/storm-program/critical-systems.html)
- [GitLab Control Framework (GCF)](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html#gitlab-control-framework-gcf)
   - [Change Management Control Family (CHG)](/handbook/engineering/security/security-assurance/security-compliance/guidance/change-management.html)
- [Information Security Policy Exception Management Process](/handbook/engineering/security/#information-security-policy-exception-management-process)
